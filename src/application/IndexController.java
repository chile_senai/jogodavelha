package application;

import java.net.URL;
import java.util.Arrays;
import java.util.Random;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class IndexController implements Initializable {
	@FXML
	Button bt00, bt01, bt02, bt10, bt11, bt12, bt20, bt21, bt22;
	@FXML
	Label lbPainel, lbX, lbO;
	Button[][] botoes;
	String[][] tabuleiro;
	String simbolo;
	Random rand;
	int placarX = 0, placarO = 0, jogadas = 0;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// cria a matriz de botoes
		botoes = new Button[3][3];
		// associa os botoes �s posi��es da matriz
		botoes[0][0] = bt00;
		botoes[0][1] = bt01;
		botoes[0][2] = bt02;
		botoes[1][0] = bt10;
		botoes[1][1] = bt11;
		botoes[1][2] = bt12;
		botoes[2][0] = bt20;
		botoes[2][1] = bt21;
		botoes[2][2] = bt22;
		// cria o tabuleiro
		tabuleiro = new String[3][3];
		// zerar tabuleiro
		zerarTabuleiro();
		// cria o random para sortear quem come�a
		rand = new Random();
		// sorteia
		sorteia();
	}

	private void zerarTabuleiro() {
		for (String[] vetor : tabuleiro) {
			Arrays.fill(vetor, "");
		}
		for (Button[] vetor : botoes) {
			for (Button b : vetor) {
				b.setText("");
			}
		}
		jogadas = 0;
	}

	private int[] getPosicao(Button button) {
		int[] retorno = new int[2];
		for (int i = 0; i < botoes.length; i++) {
			for (int j = 0; j < botoes[i].length; j++) {
				if (button == botoes[i][j]) {
					retorno[0] = i;
					retorno[1] = j;
					return retorno;
				}
			}
		}
		return retorno;
	}

	private void sorteia() {
		// sorteia quem come�a
		if (rand.nextBoolean()) {
			simbolo = "X";
		} else {
			simbolo = "O";
		}
		// exibe no painel quem come�a
		lbPainel.setText("O jogo come�a com " + simbolo);
	}

	private boolean ocupado(int linha, int coluna) {
		if (!tabuleiro[linha][coluna].equals("")) {
			return true;
		} else {
			return false;
		}
	}

	private boolean vencedor(String simbolo) {
		// verifica se venceu na linha
		for (int i = 0; i < 3; i++) {
			if (tabuleiro[i][0].equals(simbolo) && tabuleiro[i][1].equals(simbolo) && tabuleiro[i][2].equals(simbolo)) {
				return true;
			}
		}
		// verifica se venceu na coluna
		for (int i = 0; i < 3; i++) {
			if (tabuleiro[0][i].equals(simbolo) && tabuleiro[1][i].equals(simbolo) && tabuleiro[2][i].equals(simbolo)) {
				return true;
			}
		}
		// verifica se venceu na diagonal
		if (tabuleiro[0][0].equals(simbolo) && tabuleiro[1][1].equals(simbolo) && tabuleiro[2][2].equals(simbolo)) {
			return true;
		}
		if (tabuleiro[0][2].equals(simbolo) && tabuleiro[1][1].equals(simbolo) && tabuleiro[2][0].equals(simbolo)) {
			return true;
		}
		return false;
	}

	@FXML
	private void tabuleiroClick(ActionEvent e) {
		Button btClicado = (Button) e.getSource();
		int[] posicao = getPosicao(btClicado);
		int linha = posicao[0];
		int coluna = posicao[1];
		if (ocupado(linha, coluna)) {
			JOptionPane.showMessageDialog(null, "Esta posi��o j� est� ocupada", "Erro", JOptionPane.ERROR_MESSAGE);
		} else {
			jogadas++;
			btClicado.setText(simbolo);
			tabuleiro[linha][coluna] = simbolo;
			// verifica se venceu
			if (vencedor(simbolo)) {
				JOptionPane.showMessageDialog(null, "Partida vencida por " + simbolo, "Parabens",
						JOptionPane.ERROR_MESSAGE);
				pontua(simbolo);
				zerarTabuleiro();
				sorteia();
			} else {
				// verifica se deu velha
				if(jogadas == 9) {
					JOptionPane.showMessageDialog(null, "Ningu�m ganhou essa rodada", "Empate", JOptionPane.INFORMATION_MESSAGE);
					zerarTabuleiro();
					sorteia();
				}
				// inverte o s�mbolo da pr�xima jogada
				if (simbolo.equals("X")) {
					simbolo = "O";
				} else {
					simbolo = "X";
				}
				lbPainel.setText("Quem joga agora � " + simbolo);
			}

		}
	}

	private void pontua(String simbolo) {
		if (simbolo.equals("X")) {
			placarX++;
			lbX.setText("X = " + placarX);
		} else {
			placarO++;
			lbO.setText("O = " + placarO);
		}
	}
	
	@FXML
	private void reiniciarClick(ActionEvent e) {
		zerarTabuleiro();
		sorteia();
		placarO = 0;
		placarX = 0;
		lbX.setText("X = " + placarX);
		lbO.setText("O = " + placarO);
	}
	
	@FXML
	private void sairClick(ActionEvent e) {
		System.exit(0);
	}
	
	@FXML
	public void sobreClick() {
		try {
			AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("/application/Sobre.fxml"));
			Scene scene = new Scene(root,300,295);
			Stage primaryStage = new Stage();
//			primaryStage.initStyle(StageStyle.UNDECORATED);
			primaryStage.setResizable(false);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Sobre Jogo da Velha");
			primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/imagens/icone.png")));
			
			primaryStage.initModality(Modality.APPLICATION_MODAL); 
			primaryStage.showAndWait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
