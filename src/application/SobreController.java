package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class SobreController implements Initializable{
	@FXML
	Button btOk;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
	
	@FXML
	public void handleOk() {
		// get a handle to the stage
	    Stage stage = (Stage) btOk.getScene().getWindow();
	    // do what you have to do
	    stage.close();
	}

}
